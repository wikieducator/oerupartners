# oerupartners

This is a simple Python script which uses
[lxml] to scrape the [OERu.org]
partner pages to keep the descriptions and
logos up to date on [WikiEducator].

## License

MIT

[lxml]: http://lxml.de/
[OERu.org]: https://OERu.org/
[WikiEducator]: https://WikiEducator.org/
